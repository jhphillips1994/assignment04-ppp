import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Bug1Test {
	
	Punter punter;
	List<Die> dice = new ArrayList<Die>();
	Face pick;
	int bet = 1;
	int initBalance = 100;
	int initLimit = 10;
	
	Die dice1;
	Die dice2;
	Die dice3;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		punter = new Punter("Test", initBalance, initLimit);
		pick = Face.getByIndex(0);
		
		dice1 = new Die();
		while(pick.toString().equals(dice1.toString())) {
			dice1.roll();
		}
		
		dice2 = dice1;
		dice3 = dice1;
		dice.add(dice1);
		dice.add(dice2);
		dice.add(dice3);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testPlayerBalanceIsCorrectAfterLosing() {
		// Arrange
		int expected = 99;
		
		// Act
		Round.play(punter, dice, pick, bet);
		
		// Assert
		int actual = punter.getBalance();
		assertEquals(expected, actual);
	}

}
