import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Bug3Test {

	Punter punter;
	int initBalance = 100;
	int initLimit = 10;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		punter = new Punter("Test", initBalance, initLimit);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testPunterCanBetUpToTheLimit() {
		// Arrange
		
		// Act
		boolean actual = punter.balanceExceedsLimitBy(90);
		
		// Assert
		assertTrue(actual);
	}

}
