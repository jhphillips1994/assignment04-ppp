import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Bug4Test {
	
	Punter punter;
	List<Die> dice = new ArrayList<Die>();
	Face pick;
	int bet = 1;
	int initBalance = 100000;
	int initLimit = 10;
	
	Die dice1;
	Die dice2;
	Die dice3;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		punter = new Punter("Test", initBalance, initLimit);
		pick = Face.getByIndex(0);
		
		dice1 = new Die();
		dice2 = new Die();
		dice3 = new Die();
		dice.add(dice1);
		dice.add(dice2);
		dice.add(dice3);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testGameOdds() {
		// Arrange
		int roundCounter = 0;
		int totalTestRounds = 10000;
		int winCounter = 0;
		int loseCounter = 0;
		double expectedLow = 0.41;
		double expectedHigh = 0.43;
		
		// Act
		while(roundCounter < totalTestRounds) {
			int winnings = Round.play(punter, dice, pick, bet);
			if(winnings > 0) {
				winCounter++;
			} else {
				loseCounter++;
			}
			roundCounter++;
		}
		
		// Assert
		System.out.println("Win Counter: " + winCounter);
		double actual = winCounter / (double) roundCounter;
		System.out.println("Win Ratio: " + actual);
		Boolean resultTest = (actual > expectedLow && actual < expectedHigh);
		assertTrue(resultTest);
	}

}
